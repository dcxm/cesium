export default {
  CESIUM_TOKEN:
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0OThmOTY5OC1lZDQ0LTQxN2ItYjE1Yy1kNzUxZDc4OWZlMTYiLCJpZCI6ODYzNCwic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU1MjQwODYwMX0.CBj01xbZe4_gTLTP5l_AgCczSOx1lA3b7pgKNsbGKgw",
  ROUTING_NAME: {
    HOME: "home",
    HEATMAP: "heatmap",
    RADAR: "radar",
		CUSTOM_GEOMETRY: 'custom-geometry',
    ABOUT: "about"
  },
  AQI_COLOR_MAP: {
    AQI500: "rgba(133,0,40,32)",
    AQI300: "rgba(161,0,76,32)",
    AQI200: "rgba(255,0,29,32)",
    AQI150: "rgba(255,119,46,32)",
    AQI100: "rgba(255,251,81,32)",
    AQI50: "rgba(0,255,69,32)"
  },
  AQI_500: 500,
  AQI_DATABUF_NAME: {
    AQI_0_50: "aqi0-50",
    AQI_50_100: "aqi50-100",
    AQI_100_150: "aqi100-150",
    AQI_150_200: "aqi150-200",
    AQI_200_300: "aqi200-300",
    AQI_300_500: "aqi300-500",
    AQI_500_: "aqi500_"
  }
};
