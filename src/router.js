import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Consts from "./consts.js";
import util from "util";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: Consts.ROUTING_NAME.HOME,
      component: Home
    },
    {
      path: util.format("/%s", Consts.ROUTING_NAME.HEATMAP),
      name: Consts.ROUTING_NAME.HEATMAP,
      component: () => import(/* webpackChunkName: "heatmap" */ "./views/Heatmap.vue")
    },
    {
      path: util.format("/%s", Consts.ROUTING_NAME.CUSTOM_GEOMETRY),
      name: Consts.ROUTING_NAME.CUSTOM_GEOMETRY,
      component: () => import(/* webpackChunkName: "custom-geometry" */ "./views/CustomGeometry.vue")
    },
    {
      path: util.format("/%s", Consts.ROUTING_NAME.RADAR),
      name: Consts.ROUTING_NAME.RADAR,
      component: () => import(/* webpackChunkName: "radar" */ "./views/Radar.vue")
    },
    {
      path: util.format("/%s", Consts.ROUTING_NAME.ABOUT),
      name: Consts.ROUTING_NAME.ABOUT,
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ "./views/About.vue")
    }
  ]
});
